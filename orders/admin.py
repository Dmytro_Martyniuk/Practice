from django.contrib import admin
from . models import *



class ProductInOrderInline(admin.TabularInline):
    model = ProductInOrder
    extra = 0 # для рядов

class StatusAdmin(admin.ModelAdmin):

    list_display = [field.name for field in Status._meta.fields] # для всех значений

    class Meta:
        model = Status

admin.site.register(Status, StatusAdmin)


class OrderAdmin(admin.ModelAdmin):

    list_display = [field.name for field in Order._meta.fields] # для всех значений
    inlines = [ProductInOrderInline]
    class Meta:
        model = Order


admin.site.register(Order, OrderAdmin)


class ProductInOrderAdmin(admin.ModelAdmin):

    list_display = [field.name for field in ProductInOrder._meta.fields] # для всех значений

    class Meta:
        model = ProductInOrder


admin.site.register(ProductInOrder, ProductInOrderAdmin)


class ProductInBasketAdmin(admin.ModelAdmin):

    list_display = [field.name for field in ProductInBasket._meta.fields] # для всех значений

    class Meta:
        model = ProductInBasket


admin.site.register(ProductInBasket, ProductInBasketAdmin)