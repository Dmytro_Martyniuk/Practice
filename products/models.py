from django.db import models
#таблица в бд

class ProductCategory(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True, default=None)
    is_active = models.BooleanField(default=True)
    #функция определения объекта в админке

    def __str__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = 'Category Product'
        verbose_name_plural = 'Category Products'



class Product(models.Model):
    #поля в бд
    name = models.CharField(max_length=64, blank=True, null=True, default=None)
    price = models.DecimalField(max_digits= 10, decimal_places=2, default=0)
    discount = models.IntegerField(default=0)
    category = models.ForeignKey(ProductCategory, blank=True, null=True, default=None)
    description = models.TextField(blank=True, null=True, default=None)
    short_description = models.TextField(blank=True, null=True, default=None)
    is_active = models.BooleanField(default=True)
    credited = models.DateTimeField(auto_now_add=True, auto_now=False)
    update = models.DateTimeField(auto_now_add=True, auto_now=False)

    #функция определения объекта в админке
    def __str__(self):
        return "%s %s" % (self.price, self.name)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'


class ProductImage(models.Model):
    #поля в бд
    image = models.ImageField(upload_to='products_images/')
    product= models.ForeignKey(Product, blank=True, null=True, default=None)
    is_main = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    credited = models.DateTimeField(auto_now_add=True, auto_now=False)
    update = models.DateTimeField(auto_now_add=True, auto_now=False)

    #функция определения объекта в админке
    def __str__(self):
        return "%s" % self.id

    class Meta:
        verbose_name = 'Image'
        verbose_name_plural = 'Images'