from django.db import models
#таблица в бд
class Subscriber(models.Model):
    #поля в бд
    email = models.EmailField()
    name = models.CharField(max_length=128)

    #функция определения объекта в админке
    def __str__(self):
        return "Пользователь %s Email %s" % (self.name, self.email)

    class Meta:
        verbose_name = 'MySubscriber'
        verbose_name_plural = 'A lot of subscribers'