from django.contrib import admin
from . models import *



class SubscriberAdmin(admin.ModelAdmin):
    #list_display = ['name', 'email'] # в админке выводим поля с именем и мылом
    list_display = [field.name for field in Subscriber._meta.fields] # для всех значений
    #exclude = ['email'] # исключаем мыло
    #fields = ['email'] # то что показываем
    list_filter = ['name'] # фильтр по имени
    search_fields = ['name'] # фильтр по имени c инпутом

    class Meta:
        model = Subscriber


admin.site.register(Subscriber, SubscriberAdmin)
