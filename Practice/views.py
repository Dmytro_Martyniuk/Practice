from django.shortcuts import render
from . forms import SubscriberForm
from products.models import *

def Practice(request):
    form = SubscriberForm(request.POST or None)

    if request.method == "POST":
        # print(form)# смотрим что передается с запросом Post
        # print(form.cleaned_data)
        data = form.cleaned_data
        # print(data['name'])

        new_form = form.save()

    return render(request, 'Practice/Practice.html', locals())


def home(request):

    products_images = ProductImage.objects.filter(is_active=True, is_main=True, product__is_active=True)
    products_images_phones = products_images.filter(product__category_id=1)
    products_images_laptops = products_images.filter(product__category_id=2)
    return render(request, 'Practice/home.html', locals())